﻿using NUnit.Framework;
using UnityEngine;

namespace Editor
{
    public class RobotShould
    {
        private static readonly Vector2 INITIAL_POS = new Vector2(10, 642);
        private static readonly Directions NORTH_DIRECTION = Directions.North;
        private static readonly Directions EAST_DIRECTION = Directions.East;
        private static readonly Directions WEST_DIRECTION = Directions.West;
        private static readonly Directions SOUTH_DIRECTION = Directions.South;
        
        private Robot _robot;

        [Test]
        public void Set_Initial_Position()
        {
            WhenARobotIsCreated();
            ThenPositionIsEqualTo(INITIAL_POS);
        }

        [Test]
        public void Set_Direction()
        {
            WhenARobotIsCreated();
            ThenDirectionIs(NORTH_DIRECTION);
        }

        [Test]
        public void Move_Forward()
        {
            var expected = new Vector2(10, 643);
            
            GivenACreatedRobot(INITIAL_POS, NORTH_DIRECTION);
            WhenMovingForward();
            ThenPositionIsEqualTo(expected);
            ThenDirectionIs(NORTH_DIRECTION);
        }

        [Test]
        public void Move_Backwards()
        {
            var initialPos = new Vector2(521, 50);
            var expected = new Vector2(520, 50);
            
            GivenACreatedRobot(initialPos, EAST_DIRECTION);
            WhenMovingBackwards();
            ThenPositionIsEqualTo(expected);
            ThenDirectionIs(EAST_DIRECTION);
        }

        [Test]
        public void Turn_Right()
        {
            GivenACreatedRobot(INITIAL_POS, NORTH_DIRECTION);
            WhenTurningRight();
            ThenDirectionIs(EAST_DIRECTION);
        }

        [Test]
        public void Turn_Left()
        {
             GivenACreatedRobot(INITIAL_POS, WEST_DIRECTION);
             WhenTurningLeft();
             ThenDirectionIs(SOUTH_DIRECTION);
        }

        private void GivenACreatedRobot(Vector2 initialPos, Directions direction)
        {
            _robot = new Robot(initialPos, direction);
        }

        private void WhenTurningLeft()
        {
            _robot.TurnLeft();
        }

        private void WhenTurningRight()
        {
            _robot.TurnRight();
        }

        private void WhenMovingBackwards()
        {
            _robot.MoveBackwards();
        }

        private void WhenMovingForward()
        {
            _robot.MoveForward();
        }

        private void WhenARobotIsCreated()
        {
            GivenACreatedRobot(INITIAL_POS, NORTH_DIRECTION);
        }

        private void ThenPositionIsEqualTo(Vector2 expected)
        {
            Assert.AreEqual(expected, _robot.GetPosition());
        }

        private void ThenDirectionIs(Directions direction)
        {
            Assert.AreEqual(direction, _robot.GetDirection());
        }
    }

    public enum Directions
    {
        North,
        East,
        West,
        South
    }

    public class Robot
    {
        private Vector2 _pos;
        private Directions _currDirection;

        public Robot(Vector2 initialPos, Directions initialDir)
        {
            _pos = initialPos;
            _currDirection = initialDir;
        }

        public Vector2 GetPosition()
        {
            return _pos;
        }

        public Directions GetDirection()
        {
            return _currDirection;
        }

        public void MoveForward()
        {
            _pos+=Vector2.up;
        }

        public void MoveBackwards()
        {
            _pos += Vector2.left;
        }

        public void TurnRight()
        {
            _currDirection = Directions.East;
        }

        public void TurnLeft()
        {
            _currDirection = Directions.South;
        }
    }
}
